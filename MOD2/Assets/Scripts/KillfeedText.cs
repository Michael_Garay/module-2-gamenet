﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class KillfeedText : MonoBehaviourPunCallbacks
{

    [SerializeField]
    TextMeshProUGUI killFeedText;



    [PunRPC]
    public void DisplayKillFeed(PhotonMessageInfo info)
    {
        killFeedText.text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
    }

}
